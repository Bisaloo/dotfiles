" Set up plugins
call plug#begin('~/.config/nvim/plugged')
Plug 'joshdick/onedark.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'ap/vim-css-color'
Plug 'Yggdroot/indentLine'
call plug#end()

" Theme
syntax on
colorscheme onedark

" Add line number
set number relativenumber

" Highlight current line
set cursorline

" Show character limit
set colorcolumn=72,80

" Backspace through line
set backspace=indent,eol,start

" Allow cursor to go one character beyong EOL
set virtualedit=onemore

" No need to use '+' or '*' to use system clipboard
set clipboard+=unnamedplus

" Preview substitutions as you are typing them 
set inccommand=nosplit

" Edit indent guide colour
let g:indentLine_color_term = 239

match Todo /\s\+$/

